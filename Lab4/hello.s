@ Este ´e um programa ARM de exemplo que imprime
@ uma string na sa´ıda padr~ao utilizando a syscall write

.text               @inicio do trecho de codigo
    .align 4        @alinha instrucoes de 4 em 4 bytes
    .globl main     @torna o simbolo main visivel fora do arquivo
main:
    mov r0, #1      @copia o valor 1 em r0, indicando que a saida da syscall write sera em stdout
    ldr r1, =string @copia em r1 o endereco da string
    mov r2, #6      @copia em r2 o tamanho da string - note que r0,r1 e r2 sao os argumentos da syscall write
    mov r7, #4      @copia o valor 4 para r7, indicando a escolha da syscall write
    svc 0x0         @chama syscalls - no caso, a instrucao acima indica que a syscall write eh a que sera chamada
    mov r7, #1      @move o valor 1 para r7, indicando a escolha da syscall exit
    svc 0x0
    
.data               @inicio do trecho de dados
    .align 4
string: .asciz "MC404\n" @coloca a string na memoria