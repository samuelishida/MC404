#include "api_robot.h" /* Robot control API */

#define SPEED 50

/* main function */
void _start(void) 
{
    set_speed_motors(SPEED,SPEED);
    
    while(1)
    {
        if(read_sonar(3) < 1200)
            set_speed_motors(0,SPEED/2);
        else if(read_sonar(4) < 1200)
            set_speed_motors(SPEED/2,0);
        else
            set_speed_motors(SPEED,SPEED);
    }
  
}

