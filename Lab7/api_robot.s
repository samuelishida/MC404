@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@ Syscall         Numero  Parametros                      Retorno
@ read_sonar      125     r0: Identificador do sonar.     r0: Distancia lida.
@ write_motors    124     r0: Velocidade para o motor 0. 
@                         r1: Velocidade para o motor 1.  -
@ write_motor0    126     r0: Velocidade para o motor.    -
@ write_motor1    127     r0: Velocidade para o motor.    -
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

.global set_speed_motor
.global set_speed_motors
.global read_sonar
.global read_sonars

set_speed_motor:
    stmfd sp!, {r7, lr} @Salva os regs calee-save e lr
    
    add r7, r1, #126        @Identifica a syscall write_motor0 ou write_motor1
    svc 0x0                 @Faz a chamada da syscall.
                            
    ldmfd sp!, {r7, pc}
    
set_speed_motors:
    stmfd sp!, {r7, lr} @Salva os regs calee-save e lr
    
    mov r7, #124
    svc 0x0                 @Faz a chamada da syscall.       
    
    ldmfd sp!, {r7, pc}
    
read_sonar:
    stmfd sp!, {r7, lr} @Salva os regs calee-save e lr
    
    mov r7, #125
    svc 0x0
    
    ldmfd sp!, {r7, pc}
    
read_sonars:
    stmfd sp!, {r7, lr} @Salva os regs calee-save e lr
    
    mov r2, r0              @r2 contem a base do vetor
    mov r1, #0
    mov r7, #125
    
    loop:
        mov r0, r1
        svc 0x0             @Chama a syscall
        
        add r3, r2, r1, LSL #2 @r3 recebe vetor[r0]
        str r0, [r3]        @salva o valor de retorno no vetor
        
        add r1, r1, #1
        cmp r1, #16

        blt loop             @salta se r1 for menor que 16
    fim_loop:
    
    ldmfd sp!, {r7, pc}
