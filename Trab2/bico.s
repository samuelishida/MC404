.text

.global set_motor_speed
.global set_motors_speed
.global read_sonar
.global read_sonars
.global register_proximity_callback
.global add_alarm
.global get_time
.global set_time
	
.align 4
    
read_sonar:
    stmfd sp!, {r7, lr} @Salva os regs calee-save e lr
    
    mov r7, #16
    svc 0x0

    str r0, [r1]
    
    ldmfd sp!, {r7, pc}
    
read_sonars:
    stmfd sp!, {r7, lr} @Salva os regs calee-save e lr
    
    mov r2, r0              @r2 contem a base do vetor
    mov r1, #0
    mov r7, #16
    
    loop:
        mov r0, r1
        svc 0x0             @Chama a syscall
        
        add r3, r2, r1, LSL #2 @r3 recebe vetor[r0]
        str r0, [r3]        @salva o valor de retorno no vetor
        
        add r1, r1, #1
        cmp r1, #16

        blt loop             @salta se r1 for menor que 16
    fim_loop:
    
    ldmfd sp!, {r7, pc}
    
register_proximity_callback:
    stmfd sp!, {r7, lr} @Salva os regs calee-save e lr
    
    mov r7, #17
    svc 0x0
    
    ldmfd sp!, {r7, pc}

set_motor_speed:
    stmfd sp!, {r7, lr} @Salva os regs calee-save e lr
    
    add r7, r1, #18        @Identifica a syscall write_motor0 ou write_motor1
    svc 0x0                 @Faz a chamada da syscall.
                            
    ldmfd sp!, {r7, pc}
    
set_motors_speed:
    stmfd sp!, {r7, lr} @Salva os regs calee-save e lr
    
    mov r7, #19
    svc 0x0                 @Faz a chamada da syscall.       
    
    ldmfd sp!, {r7, pc}
    

get_time:
    stmfd sp!, {r7, lr} @Salva os regs calee-save e lr
    
    mov r7, #20
    svc 0x0
    
    ldmfd sp!, {r7, pc}

set_time:
    stmfd sp!, {r7, lr} @Salva os regs calee-save e lr
    
    mov r7, #21
    svc 0x0
    
    ldmfd sp!, {r7, pc}

add_alarm:
    stmfd sp!, {r7, lr} @Salva os regs calee-save e lr
    
    mov r7, #22
    svc 0x0
    
    ldmfd sp!, {r7, pc}
