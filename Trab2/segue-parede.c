#include "api_robot2.h" /* Robot control API */

#define SPEED 50

/* main function */
void _start(void) 
{
    unsigned short *dist;
    set_motors_speed(SPEED,SPEED);
    
    while(1)
    {
	read_sonar(3,dist);
        if(*dist < 1200)
            set_motors_speed(0,SPEED/2);
        else
            set_motors_speed(SPEED,SPEED);
    }
  
}
